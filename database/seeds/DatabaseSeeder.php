<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 50; $i++) {

            DB::table('cart_items')->insert([
                'cash_name' => 'cash_' . rand(0, 3),
                'article' => Str::random(10),
                'name' => Str::random(30),
                'price' => rand(1, 1000),
                'quantity' => rand(1, 10),
            ]);
        }
    }
}
