<?php

use Illuminate\Http\Request;
use App\Http\Controllers\CartItemController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::resource('/cart-item', 'CartItemController');
Route::apiResource('/recommended', 'RecommendedController');


Route::get('/indexByCash', 'CartItemController@indexByCash');
Route::get('/indexByCashNew', 'CartItemController@indexByCashNew');
Route::get('/recommendedByCash', 'RecommendedController@recommendedByCash');

Route::post('/storeAll', 'CartItemController@storeAll');

Route::delete('/destroyAll', 'CartItemController@destroyAll');
