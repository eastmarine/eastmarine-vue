<?php

namespace App\Http\Controllers;

use App\CartItem;
use App\Http\Requests\RecommendedRequest;
use App\Recommended;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class RecommendedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return Recommended::all();
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function recommendedByCash(Request $request)
    {
        return Recommended::where('cash_name', $request->input('q'))->orderBy('id')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param RecommendedRequest $request
     * @return Response
     */
    public function store(RecommendedRequest $request)
    {
        $recommended = Recommended::create($request->all());
        return response()->json($recommended);
    }


    /**
     * Display the specified resource.
     *
     * @param \App\Recommended $recommended
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    public function show(Recommended $recommended)
    {
        return Recommended::findOrFail($recommended);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param RecommendedRequest $request
     * @param \App\Recommended $recommended
     * @return Response
     */
    public function update(RecommendedRequest $request, Recommended $recommended)
    {
        $recommend = Recommended::findOrFail($recommended);
        $recommend->fill($request->except(['id']));
        $recommend->save();
        return response()->json($recommend);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Recommended $recommended
     * @return Response
     * @throws \Exception
     */
    public function destroy(Recommended $recommended)
    {
        $recommend = Recommended::findOrFail($recommended);
        if ($recommend->delete())
            return response(null, 204);
    }
}
