<?php

namespace App\Http\Controllers;

use App\CartItem;
use App\Http\Requests\CartItemRequest;
use App\Recommended;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CartItemController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return CartItem::orderByDesc('id')->get();
    }


    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function indexByCash(Request $request)
    {
        return CartItem::where('cash_name', $request->input('q'))->where('quantity', '>', 0)->orderByDesc('id')->get();
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function indexByCashNew(Request $request)
    {
        $cartItems = CartItem::where('cash_name', $request->input('q'))->orderByDesc('id')->get();
        return $cartItems;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param CartItemRequest $request
     * @return CartItem
     */
    public function store(CartItemRequest $request)
    {
        $cartItem = CartItem::where('cash_name', $request->input('cash_name'))->where('article', $request->input('article'))->first();
        if (empty($cartItem)) {
            $cartItem = CartItem::create($request->all());
            return response()->json($cartItem);
        }
        $cartItem->quantity = $cartItem->quantity + 1;
        $cartItem->from_recommend = 1;
        $cartItem->update();
        return response()->json($cartItem);
    }

    /**
     * Display the specified resource.
     *
     * @param CartItem $cartItem
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    public function show(CartItem $cartItem)
    {
        return CartItem::findOrFail($cartItem);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $cartItems = CartItem::findOrFail($id);
//        $cartItems->fill($request->except(['id']));
        $cartItems->quantity = $cartItems->quantity + $request->input('quantity');
        if($cartItems->quantity < 0){
            $cartItems->quantity  = 0;
        }
		if($cartItems->quantity > $cartItems->limit_stock){
            $cartItems->quantity  = $cartItems->limit_stock;
        }
        $cartItems->from_recommend = 1;
        $cartItems->save();

        return response()->json($cartItems);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return Response
     * @throws Exception
     */
    public function destroy($id)
    {
        $cartItem = CartItem::findOrFail($id);
        if ($cartItem) {
            $cartItem->delete();
        }
        return response(null, 204);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return CartItem
     * @throws Exception
     */
    public function storeAll(Request $request)
    {
        $this->destroyByCash($request, true);

        foreach ($request->all() as $item) {
            if (is_array($item))
                Recommended::create($item);
        }
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function destroyAll(Request $request)
    {
        $this->destroyByCash($request);
        return response(null, 204);
    }

    private function destroyByCash($request, $onlyRecommend = false)
    {
        if (!empty($request->input('q')) || !empty($request->input('cash_name'))) {
            $cash_name = empty($request->input('q')) ? $request->input('cash_name') : $request->input('q');

            if (!$onlyRecommend) {
                $cartItems = CartItem::where('cash_name', $cash_name)->orderByDesc('id')->get();
                /** @var CartItem $item */
                foreach ($cartItems as $item) {
                    if (!empty($item))
                        $item->delete();
                }
            }

            $recommended = Recommended::where('cash_name', $cash_name)->orderByDesc('id')->get();
            /** @var Recommended $itemR */
            foreach ($recommended as $itemR) {
                if (!empty($itemR))
                    $itemR->delete();
            }
        }
    }

}
