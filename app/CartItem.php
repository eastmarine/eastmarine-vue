<?php

namespace App;

/**
 * This is the model class for table "{{%products}}".
 *
 * @var int $id
 * @var string $cash_name
 * @var string $article
 * @var string $image
 * @var string $name
 * @var string $price
 * @var string $quantity
 * @var string $limit_stock
 *
 */

use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{
    //
    protected $table = 'cart_items';

    protected $fillable = ['cash_name', 'image', 'article', 'name', 'price', 'quantity', 'from_recommend', 'limit_stock'];

    protected $hidden = ['created_at', 'updated_at'];

    public static function findByArticleAndCash($cash_name, $article)
    {
        $cartItems = CartItem::where('cash_name', $cash_name)->where('article', $article)->first();
        if(!empty($cartItems)){
            return $cartItems;
        }
        return null;
    }

}
