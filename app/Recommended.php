<?php

namespace App;

/**
 * This is the model class for table "{{%products}}".
 *
 * @var int $id
 * @var string $image
 * @var string $article
 * @var string $name
 * @var string $limit_stock
 *
 */


use Illuminate\Database\Eloquent\Model;

class Recommended extends Model
{
    protected $table = 'recommended';

    protected $fillable = ['article', 'image', 'name', 'cash_name', 'price' ,'limit_stock'];

}
