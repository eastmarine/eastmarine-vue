/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
window.Vuetify = require('vuetify');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import VueRouter from 'vue-router';
import axios from 'axios'
import VueAxios from 'vue-axios'
import {VContainer, VFlex, VDialog, VLayout, VCard, VBtn, VBtnToggle} from 'vuetify/lib'
import welcome from './components/App';
import store from './store/index';
import Vuetify from 'vuetify';
import VueProgressiveImage from 'vue-progressive-image'

Vue.use(VueProgressiveImage);

Vue.use(VueRouter);
Vue.use(VueAxios, axios);
Vue.use(Vuetify);

const router = new VueRouter({mode: 'history'});

const vuetifyOptions = { };

new Vue({
    el: '#app',
    components: {
        welcome,
        VueProgressiveImage,
        VContainer,
        VFlex,
        VDialog,
        VLayout,
        VCard,
        VBtn,
        VBtnToggle,
    },
    vuetify: new Vuetify(vuetifyOptions),
    router: router,
    store: store,
});
