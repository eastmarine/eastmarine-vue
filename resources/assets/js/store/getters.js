let getters = {
    cartItem: state => {
        return state.cartItem;
    },
    recommended: state => {
        return state.recommended;
    }
};

export default  getters
