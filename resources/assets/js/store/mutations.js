let mutations = {
    ADD_CART_ITEM(state, post) {
        // state.cartItem.unshift(post)
    },
    UPDATE_CART_ITEM(state, post) {
        return state.cartItem = post
    },
    GET_CART_ITEM(state, cartItems) {
        return state.cartItem = cartItems
    },
    GET_RECOMMENDED(state, recommended) {
        return state.recommended = recommended
    },
    ADD_TO_CART_ITEM(state, post) {
        state.cartItem.unshift(post)
    },
};
export default mutations
