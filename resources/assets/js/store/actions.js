let actions = {
    addCartItem({commit}, post) {
        Vue.axios.post('/api/cart-item', post)
            .then(res => {
                commit('ADD_CART_ITEM', res.data)
            }).catch(err => {
            console.log(err)
        })
    },
    getCartItem({commit}, {self, route}) {
        if (route.query.q)
            Vue.axios.get('/api/indexByCash?q=' + route.query.q)
                .then(res => {
                    commit('GET_CART_ITEM', res.data);
                    self.filterCartItem();
                }).catch(err => {
                console.log(err)
            })
    },
    getRecommendedItem({commit}, {self, route}) {
        if (route.query.q)
            Vue.axios.get('/api/recommendedByCash?q=' + route.query.q)
                .then(res => {
                    commit('GET_RECOMMENDED', res.data);
                    self.filterRecommended();
                }).catch(err => {
                console.log(err)
            })
    },
    addRecommendedToCart({commit}, {post, route}) {
        if (route.query.q)
            Vue.axios.post('/api/cart-item', {
                cash_name: route.query.q,
                article: post['article'],
                name: post['name'],
                price: post['price'],
                image: post['image'],
                limit_stock: post['limit_stock'],
                quantity: 1,
                from_recommend: 1
            }).then(res => {
                // commit('ADD_TO_CART_ITEM', res.data)
            }).catch(err => {
                console.log(err)
            })
    },
    updateCartItem({commit}, {post, id}) {
        Vue.axios.put('/api/cart-item/' + id, post)
            .then(res => {
                commit('UPDATE_CART_ITEM')
            }).catch(err => {
            console.log(err)
        })
    },
};

export default actions
